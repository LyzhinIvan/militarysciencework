﻿namespace MilitaryScienceWork
{
    partial class AccurateCalcTable
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblProbability = new System.Windows.Forms.Label();
            this.txtArgument = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.chkUsing = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblProbability
            // 
            this.lblProbability.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblProbability.Location = new System.Drawing.Point(3, 55);
            this.lblProbability.Name = "lblProbability";
            this.lblProbability.Size = new System.Drawing.Size(100, 22);
            this.lblProbability.TabIndex = 9;
            // 
            // txtArgument
            // 
            this.txtArgument.Location = new System.Drawing.Point(3, 27);
            this.txtArgument.Name = "txtArgument";
            this.txtArgument.Size = new System.Drawing.Size(100, 20);
            this.txtArgument.TabIndex = 8;
            this.txtArgument.TextChanged += new System.EventHandler(this.txtArgument_TextChanged);
            // 
            // lblName
            // 
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblName.Location = new System.Drawing.Point(3, 2);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(100, 22);
            this.lblName.TabIndex = 10;
            this.lblName.Text = "Земля";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkUsing
            // 
            this.chkUsing.AutoSize = true;
            this.chkUsing.Location = new System.Drawing.Point(49, 84);
            this.chkUsing.Name = "chkUsing";
            this.chkUsing.Size = new System.Drawing.Size(15, 14);
            this.chkUsing.TabIndex = 11;
            this.chkUsing.UseVisualStyleBackColor = true;
            this.chkUsing.CheckedChanged += new System.EventHandler(this.chkUsing_CheckedChanged);
            // 
            // AccurateCalcTable
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Lavender;
            this.Controls.Add(this.chkUsing);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblProbability);
            this.Controls.Add(this.txtArgument);
            this.Name = "AccurateCalcTable";
            this.Size = new System.Drawing.Size(108, 109);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblProbability;
        private System.Windows.Forms.TextBox txtArgument;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.CheckBox chkUsing;
    }
}
