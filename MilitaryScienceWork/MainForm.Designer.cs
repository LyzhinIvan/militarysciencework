﻿namespace MilitaryScienceWork
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.graphControl = new ZedGraph.ZedGraphControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDeleteGraph = new System.Windows.Forms.Button();
            this.btnBuildGraph = new System.Windows.Forms.Button();
            this.txtF = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtMaxDn = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMinDn = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtG = new System.Windows.Forms.TextBox();
            this.txtTfp = new System.Windows.Forms.TextBox();
            this.txtTob = new System.Windows.Forms.TextBox();
            this.txtTa = new System.Windows.Forms.TextBox();
            this.txtRd = new System.Windows.Forms.TextBox();
            this.txtRf = new System.Windows.Forms.TextBox();
            this.txtRo = new System.Windows.Forms.TextBox();
            this.txtSob = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSe = new System.Windows.Forms.TextBox();
            this.panelSelectGraph = new System.Windows.Forms.Panel();
            this.btnGraph4 = new System.Windows.Forms.Button();
            this.btnGraph3 = new System.Windows.Forms.Button();
            this.btnGraph2 = new System.Windows.Forms.Button();
            this.btnGraph1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblTotalProbability = new System.Windows.Forms.Label();
            this.tableSpace = new MilitaryScienceWork.AccurateCalcTable();
            this.tableCeil = new MilitaryScienceWork.AccurateCalcTable();
            this.tableSea = new MilitaryScienceWork.AccurateCalcTable();
            this.tableGround = new MilitaryScienceWork.AccurateCalcTable();
            this.groupBox1.SuspendLayout();
            this.panelSelectGraph.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // graphControl
            // 
            this.graphControl.Location = new System.Drawing.Point(12, 10);
            this.graphControl.Name = "graphControl";
            this.graphControl.ScrollGrace = 0D;
            this.graphControl.ScrollMaxX = 0D;
            this.graphControl.ScrollMaxY = 0D;
            this.graphControl.ScrollMaxY2 = 0D;
            this.graphControl.ScrollMinX = 0D;
            this.graphControl.ScrollMinY = 0D;
            this.graphControl.ScrollMinY2 = 0D;
            this.graphControl.Size = new System.Drawing.Size(500, 375);
            this.graphControl.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Info;
            this.groupBox1.Controls.Add(this.btnDeleteGraph);
            this.groupBox1.Controls.Add(this.btnBuildGraph);
            this.groupBox1.Controls.Add(this.txtF);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtMaxDn);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtMinDn);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtG);
            this.groupBox1.Controls.Add(this.txtTfp);
            this.groupBox1.Controls.Add(this.txtTob);
            this.groupBox1.Controls.Add(this.txtTa);
            this.groupBox1.Controls.Add(this.txtRd);
            this.groupBox1.Controls.Add(this.txtRf);
            this.groupBox1.Controls.Add(this.txtRo);
            this.groupBox1.Controls.Add(this.txtSob);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtSe);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.groupBox1.Location = new System.Drawing.Point(521, 60);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(460, 325);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Исходные данные - Земля";
            // 
            // btnDeleteGraph
            // 
            this.btnDeleteGraph.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnDeleteGraph.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnDeleteGraph.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDeleteGraph.Location = new System.Drawing.Point(236, 291);
            this.btnDeleteGraph.Name = "btnDeleteGraph";
            this.btnDeleteGraph.Size = new System.Drawing.Size(215, 34);
            this.btnDeleteGraph.TabIndex = 26;
            this.btnDeleteGraph.Text = "Стереть график";
            this.btnDeleteGraph.UseVisualStyleBackColor = false;
            this.btnDeleteGraph.Click += new System.EventHandler(this.btnDeleteGraph_Click);
            // 
            // btnBuildGraph
            // 
            this.btnBuildGraph.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnBuildGraph.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnBuildGraph.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnBuildGraph.Location = new System.Drawing.Point(9, 291);
            this.btnBuildGraph.Name = "btnBuildGraph";
            this.btnBuildGraph.Size = new System.Drawing.Size(215, 34);
            this.btnBuildGraph.TabIndex = 25;
            this.btnBuildGraph.Text = "Построить график";
            this.btnBuildGraph.UseVisualStyleBackColor = false;
            this.btnBuildGraph.Click += new System.EventHandler(this.btnBuildGraph_Click);
            // 
            // txtF
            // 
            this.txtF.Location = new System.Drawing.Point(351, 215);
            this.txtF.Name = "txtF";
            this.txtF.Size = new System.Drawing.Size(100, 22);
            this.txtF.TabIndex = 8;
            this.txtF.Enter += new System.EventHandler(this.txtInput_Enter);
            this.txtF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDoubleChecker);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 220);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(171, 16);
            this.label13.TabIndex = 24;
            this.label13.Text = "Фокусное расстояние";
            // 
            // txtMaxDn
            // 
            this.txtMaxDn.Location = new System.Drawing.Point(351, 265);
            this.txtMaxDn.Name = "txtMaxDn";
            this.txtMaxDn.Size = new System.Drawing.Size(100, 22);
            this.txtMaxDn.TabIndex = 11;
            this.txtMaxDn.Enter += new System.EventHandler(this.txtInput_Enter);
            this.txtMaxDn.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDoubleChecker);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(317, 270);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(26, 16);
            this.label12.TabIndex = 21;
            this.label12.Text = "до";
            // 
            // txtMinDn
            // 
            this.txtMinDn.Location = new System.Drawing.Point(206, 265);
            this.txtMinDn.Name = "txtMinDn";
            this.txtMinDn.Size = new System.Drawing.Size(100, 22);
            this.txtMinDn.TabIndex = 10;
            this.txtMinDn.Enter += new System.EventHandler(this.txtInput_Enter);
            this.txtMinDn.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDoubleChecker);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(174, 270);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 16);
            this.label11.TabIndex = 19;
            this.label11.Text = "от";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 270);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(161, 16);
            this.label10.TabIndex = 18;
            this.label10.Text = "Дальность разведки";
            // 
            // txtG
            // 
            this.txtG.Location = new System.Drawing.Point(351, 240);
            this.txtG.Name = "txtG";
            this.txtG.Size = new System.Drawing.Size(100, 22);
            this.txtG.TabIndex = 9;
            this.txtG.Enter += new System.EventHandler(this.txtInput_Enter);
            this.txtG.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDoubleChecker);
            // 
            // txtTfp
            // 
            this.txtTfp.Location = new System.Drawing.Point(351, 190);
            this.txtTfp.Name = "txtTfp";
            this.txtTfp.Size = new System.Drawing.Size(100, 22);
            this.txtTfp.TabIndex = 7;
            this.txtTfp.Enter += new System.EventHandler(this.txtInput_Enter);
            this.txtTfp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDoubleChecker);
            // 
            // txtTob
            // 
            this.txtTob.Location = new System.Drawing.Point(351, 165);
            this.txtTob.Name = "txtTob";
            this.txtTob.Size = new System.Drawing.Size(100, 22);
            this.txtTob.TabIndex = 6;
            this.txtTob.Enter += new System.EventHandler(this.txtInput_Enter);
            this.txtTob.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDoubleChecker);
            // 
            // txtTa
            // 
            this.txtTa.Location = new System.Drawing.Point(351, 140);
            this.txtTa.Name = "txtTa";
            this.txtTa.Size = new System.Drawing.Size(100, 22);
            this.txtTa.TabIndex = 5;
            this.txtTa.Enter += new System.EventHandler(this.txtInput_Enter);
            this.txtTa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDoubleChecker);
            // 
            // txtRd
            // 
            this.txtRd.Location = new System.Drawing.Point(351, 115);
            this.txtRd.Name = "txtRd";
            this.txtRd.Size = new System.Drawing.Size(100, 22);
            this.txtRd.TabIndex = 4;
            this.txtRd.Enter += new System.EventHandler(this.txtInput_Enter);
            this.txtRd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDoubleChecker);
            // 
            // txtRf
            // 
            this.txtRf.Location = new System.Drawing.Point(351, 90);
            this.txtRf.Name = "txtRf";
            this.txtRf.Size = new System.Drawing.Size(100, 22);
            this.txtRf.TabIndex = 3;
            this.txtRf.Enter += new System.EventHandler(this.txtInput_Enter);
            this.txtRf.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDoubleChecker);
            // 
            // txtRo
            // 
            this.txtRo.Location = new System.Drawing.Point(351, 65);
            this.txtRo.Name = "txtRo";
            this.txtRo.Size = new System.Drawing.Size(100, 22);
            this.txtRo.TabIndex = 2;
            this.txtRo.Enter += new System.EventHandler(this.txtInput_Enter);
            this.txtRo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDoubleChecker);
            // 
            // txtSob
            // 
            this.txtSob.Location = new System.Drawing.Point(351, 40);
            this.txtSob.Name = "txtSob";
            this.txtSob.Size = new System.Drawing.Size(100, 22);
            this.txtSob.TabIndex = 1;
            this.txtSob.Enter += new System.EventHandler(this.txtInput_Enter);
            this.txtSob.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDoubleChecker);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 245);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(227, 16);
            this.label9.TabIndex = 9;
            this.label9.Text = "Коэффициент контрастности";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 195);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(329, 16);
            this.label8.TabIndex = 8;
            this.label8.Text = "Значение простр.-частот. хар. фотопленки";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 170);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(318, 16);
            this.label7.TabIndex = 7;
            this.label7.Text = "Значение простр.-частот. хар. объектива";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(300, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "Коэффициент пропускания атмосферы";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(218, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Коэффициент яркости фона";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(225, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Коэффициент яркости дымки";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(241, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Коэффициент яркости объекта";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(320, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Площадь наблюдаемой проекции объекта";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(243, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Площадь элемента разрешения";
            // 
            // txtSe
            // 
            this.txtSe.Location = new System.Drawing.Point(351, 15);
            this.txtSe.Name = "txtSe";
            this.txtSe.Size = new System.Drawing.Size(100, 22);
            this.txtSe.TabIndex = 0;
            this.txtSe.Enter += new System.EventHandler(this.txtInput_Enter);
            this.txtSe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDoubleChecker);
            // 
            // panelSelectGraph
            // 
            this.panelSelectGraph.Controls.Add(this.btnGraph4);
            this.panelSelectGraph.Controls.Add(this.btnGraph3);
            this.panelSelectGraph.Controls.Add(this.btnGraph2);
            this.panelSelectGraph.Controls.Add(this.btnGraph1);
            this.panelSelectGraph.Location = new System.Drawing.Point(521, 11);
            this.panelSelectGraph.Name = "panelSelectGraph";
            this.panelSelectGraph.Size = new System.Drawing.Size(459, 49);
            this.panelSelectGraph.TabIndex = 3;
            // 
            // btnGraph4
            // 
            this.btnGraph4.BackColor = System.Drawing.Color.Indigo;
            this.btnGraph4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnGraph4.Location = new System.Drawing.Point(352, 3);
            this.btnGraph4.Name = "btnGraph4";
            this.btnGraph4.Size = new System.Drawing.Size(100, 40);
            this.btnGraph4.TabIndex = 3;
            this.btnGraph4.Text = "Космос";
            this.btnGraph4.UseVisualStyleBackColor = false;
            this.btnGraph4.Click += new System.EventHandler(this.btnSelectGraph_Click);
            // 
            // btnGraph3
            // 
            this.btnGraph3.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnGraph3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnGraph3.Location = new System.Drawing.Point(237, 3);
            this.btnGraph3.Name = "btnGraph3";
            this.btnGraph3.Size = new System.Drawing.Size(100, 40);
            this.btnGraph3.TabIndex = 2;
            this.btnGraph3.Text = "Воздух";
            this.btnGraph3.UseVisualStyleBackColor = false;
            this.btnGraph3.Click += new System.EventHandler(this.btnSelectGraph_Click);
            // 
            // btnGraph2
            // 
            this.btnGraph2.BackColor = System.Drawing.Color.Cyan;
            this.btnGraph2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnGraph2.Location = new System.Drawing.Point(122, 3);
            this.btnGraph2.Name = "btnGraph2";
            this.btnGraph2.Size = new System.Drawing.Size(100, 40);
            this.btnGraph2.TabIndex = 1;
            this.btnGraph2.Text = "Море";
            this.btnGraph2.UseVisualStyleBackColor = false;
            this.btnGraph2.Click += new System.EventHandler(this.btnSelectGraph_Click);
            // 
            // btnGraph1
            // 
            this.btnGraph1.BackColor = System.Drawing.Color.SpringGreen;
            this.btnGraph1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGraph1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnGraph1.Location = new System.Drawing.Point(7, 3);
            this.btnGraph1.Name = "btnGraph1";
            this.btnGraph1.Size = new System.Drawing.Size(100, 40);
            this.btnGraph1.TabIndex = 0;
            this.btnGraph1.Text = "Земля";
            this.btnGraph1.UseVisualStyleBackColor = false;
            this.btnGraph1.Click += new System.EventHandler(this.btnSelectGraph_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Bisque;
            this.groupBox2.Controls.Add(this.lblTotalProbability);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.tableSpace);
            this.groupBox2.Controls.Add(this.tableCeil);
            this.groupBox2.Controls.Add(this.tableSea);
            this.groupBox2.Controls.Add(this.tableGround);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(12, 399);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(968, 134);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Комплексная разведка";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(6, 47);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 20);
            this.label14.TabIndex = 5;
            this.label14.Text = "Дальность";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(6, 76);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 20);
            this.label15.TabIndex = 6;
            this.label15.Text = "Вероятность";
            // 
            // lblTotalProbability
            // 
            this.lblTotalProbability.AutoSize = true;
            this.lblTotalProbability.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTotalProbability.Location = new System.Drawing.Point(695, 47);
            this.lblTotalProbability.Name = "lblTotalProbability";
            this.lblTotalProbability.Size = new System.Drawing.Size(110, 29);
            this.lblTotalProbability.TabIndex = 7;
            this.lblTotalProbability.Text = "Робщ=0";
            // 
            // tableSpace
            // 
            this.tableSpace.BackColor = System.Drawing.Color.Lavender;
            this.tableSpace.Checked = false;
            this.tableSpace.Distance = 0D;
            this.tableSpace.Location = new System.Drawing.Point(544, 21);
            this.tableSpace.Name = "tableSpace";
            this.tableSpace.Size = new System.Drawing.Size(108, 109);
            this.tableSpace.TabIndex = 4;
            // 
            // tableCeil
            // 
            this.tableCeil.BackColor = System.Drawing.Color.Lavender;
            this.tableCeil.Checked = false;
            this.tableCeil.Distance = 0D;
            this.tableCeil.Location = new System.Drawing.Point(406, 21);
            this.tableCeil.Name = "tableCeil";
            this.tableCeil.Size = new System.Drawing.Size(108, 109);
            this.tableCeil.TabIndex = 3;
            // 
            // tableSea
            // 
            this.tableSea.BackColor = System.Drawing.Color.Lavender;
            this.tableSea.Checked = false;
            this.tableSea.Distance = 0D;
            this.tableSea.Location = new System.Drawing.Point(267, 21);
            this.tableSea.Name = "tableSea";
            this.tableSea.Size = new System.Drawing.Size(108, 109);
            this.tableSea.TabIndex = 2;
            // 
            // tableGround
            // 
            this.tableGround.BackColor = System.Drawing.Color.Lavender;
            this.tableGround.Checked = false;
            this.tableGround.Distance = 0D;
            this.tableGround.Location = new System.Drawing.Point(132, 21);
            this.tableGround.Name = "tableGround";
            this.tableGround.Size = new System.Drawing.Size(108, 109);
            this.tableGround.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(993, 561);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panelSelectGraph);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.graphControl);
            this.Name = "MainForm";
            this.Text = "Анализ фотографической разведки";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelSelectGraph.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ZedGraph.ZedGraphControl graphControl;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSe;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtMaxDn;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtMinDn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtG;
        private System.Windows.Forms.TextBox txtTfp;
        private System.Windows.Forms.TextBox txtTob;
        private System.Windows.Forms.TextBox txtTa;
        private System.Windows.Forms.TextBox txtRd;
        private System.Windows.Forms.TextBox txtRf;
        private System.Windows.Forms.TextBox txtRo;
        private System.Windows.Forms.TextBox txtSob;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtF;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panelSelectGraph;
        private System.Windows.Forms.Button btnGraph3;
        private System.Windows.Forms.Button btnGraph2;
        private System.Windows.Forms.Button btnGraph1;
        private System.Windows.Forms.Button btnBuildGraph;
        private System.Windows.Forms.Button btnDeleteGraph;
        private System.Windows.Forms.Button btnGraph4;
        private System.Windows.Forms.GroupBox groupBox2;
        private AccurateCalcTable tableCeil;
        private AccurateCalcTable tableSea;
        private AccurateCalcTable tableGround;
        private AccurateCalcTable tableSpace;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblTotalProbability;
    }
}

