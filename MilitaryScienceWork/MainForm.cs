﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;
using System.Globalization;

namespace MilitaryScienceWork
{
    public partial class MainForm : Form
    {
        Calculator[] calcs = new Calculator[4];
        LineItem[] curves = new LineItem[4];
        int activePanel = 0;

        public MainForm()
        {
            InitializeComponent();
            for (int i = 0; i < calcs.Length; ++i)
                calcs[i] = new Calculator();
            tableGround.Text = "Земля";
            tableSea.Text = "Море";
            tableCeil.Text = "Воздух";
            tableSpace.Text = "Космос";
            tableGround.calculator = calcs[0];
            tableSea.calculator = calcs[1];
            tableCeil.calculator = calcs[2];
            tableSpace.calculator = calcs[3];
            tableGround.Changed += table_Changed;
            tableSea.Changed += table_Changed;
            tableCeil.Changed += table_Changed;
            tableSpace.Changed += table_Changed;
        }

        private void table_Changed(object sender, EventArgs e)
        {
            List<double> values = new List<double>();
            if (tableGround.Checked)
                values.Add(tableGround.GetProbability());
            if (tableSea.Checked)
                values.Add(tableSea.GetProbability());
            if (tableCeil.Checked)
                values.Add(tableCeil.GetProbability());
            if (tableSpace.Checked)
                values.Add(tableSpace.GetProbability());
            if (values.Count == 0)
                lblTotalProbability.Text = "Робщ=0.00000";
            else
            {
                double total = 1;
                foreach (var p in values)
                    total *= p;
                lblTotalProbability.Text = String.Format("Робщ={0:0.00000}", total);
            }
                
        }

        /// <summary>
        /// Инициализация координатной плоскости для графика
        /// </summary>
        private void initGraph()
        {
            GraphPane myPane = new GraphPane();
            graphControl.GraphPane = myPane;
            myPane.XAxis.Title.Text = "Дальность разведки";//подпись оси X
            myPane.YAxis.Title.Text = "Вероятность обнаружения";//подпись оси Y
            myPane.Title.Text = "График функции вероятности обнаружения";//подпись графика
            myPane.Fill = new Fill(Color.White, Color.LightYellow, 45.0f);//фон графика заливаем градиентом
            myPane.Chart.Fill.Type = FillType.None;
            myPane.Legend.Position = LegendPos.Float;
            myPane.Legend.IsHStack = false;
            myPane.YAxis.Scale.MinAuto = true;
            myPane.YAxis.Scale.MaxAuto = true;
            myPane.IsBoundedRanges = true;
        }

        //Загрузка формы
        private void MainForm_Load(object sender, EventArgs e)
        {
            initGraph();
            fillInputPanel(calcs[0]);
        }

        /// <summary>
        /// Нажатие на кнопку "Построить график"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBuildGraph_Click(object sender, EventArgs e)
        {
            Calculator activeCalc = calcs[activePanel];
            if(readValues(activeCalc)==false)
            {
                MessageBox.Show("Ошибка в исходных данных!");
                return;
            }
            graphControl.GraphPane.CurveList.Remove(curves[activePanel]);
            Dictionary<double, double> coordinats = new Dictionary<double, double>();// coordinats-хранит координаты точек функции
            for (double d = activeCalc.MinDn; d <= activeCalc.MaxDn; d += 0.1)
            {
                coordinats.Add(d, Math.Min(1,activeCalc.Pob(d)));//расчитываем координаты
            }
            double[] x = coordinats.Select(p => p.Key).ToArray();
            double[] y = coordinats.Select(p => p.Value).ToArray();
            Color[] colors = new Color[4] { Color.SpringGreen, Color.Cyan, Color.Blue, Color.Indigo };
            LineItem newCurve = graphControl.GraphPane.AddCurve("", x, y, colors[activePanel], SymbolType.None);
            curves[activePanel] = newCurve;
            graphControl.GraphPane.YAxis.Scale.MaxAuto = false;
            graphControl.GraphPane.YAxis.Scale.Max = 1.1;
            graphControl.AxisChange();
            graphControl.Refresh();
            graphControl.Visible = true;
        }

        /// <summary>
        /// Считывает вводимые данные с формы в переменную calc
        /// </summary>
        /// <returns>Возвращает true, если не было ошибок в исходных данных</returns>
        private bool readValues(Calculator calc)
        {
            try
            {
                //Чтение исходных данных
                CultureInfo culture = CultureInfo.InvariantCulture;
                calc.F = Double.Parse(txtF.Text, culture);
                calc.G = Double.Parse(txtG.Text, culture);
                calc.MaxDn = Double.Parse(txtMaxDn.Text, culture);
                calc.MinDn = Double.Parse(txtMinDn.Text, culture);
                calc.Rd = Double.Parse(txtRd.Text, culture);
                calc.Rf = Double.Parse(txtRf.Text, culture);
                calc.Ro = Double.Parse(txtRo.Text, culture);
                calc.Se = Double.Parse(txtSe.Text, culture);
                calc.Sob = Double.Parse(txtSob.Text, culture);
                calc.Ta = Double.Parse(txtTa.Text, culture);
                calc.Tfp = Double.Parse(txtTfp.Text, culture);
                calc.Tob = Double.Parse(txtTob.Text, culture);
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Запрещает ввод букв и других символов, не соответсвующих типу double
        /// </summary>
        /// <param name="sender">Textbox, в котором произошло нажатие клавиши</param>
        /// <param name="e">Параметры ввода</param>
        private void txtDoubleChecker(object sender, KeyPressEventArgs e)
        {
            //Цифры и backspace
            if (Char.IsDigit(e.KeyChar) || e.KeyChar==8) return;
            //Разделитель целой и дробной частей
            bool containsDot = ((TextBox)sender).Text.Contains('.');
            if (e.KeyChar == ',') e.KeyChar = '.'; 
            if (e.KeyChar == '.')
                if (containsDot)
                    e.Handled = true;
                else if (((TextBox)sender).Text.Length == 0)
                    e.Handled = true;
                else return;
            //Знак минус
            if (e.KeyChar == '-')
                if (((TextBox)sender).Text.Length == 0)
                    return;
                else e.Handled = true;
            //Остальные случаи
            e.Handled = true;
        }

        /// <summary>
        /// Выделение всего текста при активации TextBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtInput_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        private void btnSelectGraph_Click(object sender, EventArgs e)
        {
            activePanel = Int32.Parse((sender as Button).Name[8].ToString()) - 1;
            groupBox1.Text = "Исходные данные - " + (sender as Button).Text;
            foreach (Button but in panelSelectGraph.Controls)
                but.FlatStyle = FlatStyle.Standard;
            (sender as Button).FlatStyle = FlatStyle.Popup;
            fillInputPanel(calcs[activePanel]);
        }

        private void fillInputPanel(Calculator calc)
        {
            txtF.Text = calc.F.ToString();
            txtG.Text = calc.G.ToString();
            txtMaxDn.Text = calc.MaxDn.ToString();
            txtMinDn.Text = calc.MinDn.ToString();
            txtRd.Text = calc.Rd.ToString();
            txtRf.Text = calc.Rf.ToString();
            txtRo.Text = calc.Ro.ToString();
            txtSe.Text = calc.Se.ToString();
            txtSob.Text = calc.Sob.ToString();
            txtTa.Text = calc.Ta.ToString();
            txtTfp.Text = calc.Tfp.ToString();
            txtTob.Text = calc.Tob.ToString();
        }

        private void btnDeleteGraph_Click(object sender, EventArgs e)
        {
            graphControl.GraphPane.CurveList.Remove(curves[activePanel]);
            graphControl.Refresh();
        }

    }
}
