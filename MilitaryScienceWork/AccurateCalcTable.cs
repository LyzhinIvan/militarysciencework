﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MilitaryScienceWork
{
    public partial class AccurateCalcTable : UserControl
    {
        public Calculator calculator;
        public event EventHandler Changed;

        public AccurateCalcTable()
        {
            InitializeComponent();
            calculator = null;
        }

        public void Clear()
        {
            txtArgument.Text = lblProbability.Text  = "";
            chkUsing.Checked = false;
        }

        public double Distance
        {
            get
            {
                double arg;
                if (Double.TryParse(txtArgument.Text, out arg))
                    return arg;
                else return 0;
            }
            set
            {
                txtArgument.Text = value.ToString();
                SetTxtValue(calculator, lblProbability);
                if (Changed != null)
                    Changed(this, EventArgs.Empty);
            }
        }

        private void SetTxtValue(Calculator calc, Label label)
        {
            if(calc==null)
            {
                label.Text = "";
                return;
            }
            double arg;
            if (Double.TryParse(txtArgument.Text, out arg))
                label.Text = calc.Pob(arg).ToString();
            else
                label.Text = "";
        }

        private void txtArgument_TextChanged(object sender, EventArgs e)
        {
            SetTxtValue(calculator, lblProbability);
            if (Changed != null)
                Changed(this, EventArgs.Empty);
        }

        public void RefreshValues()
        {
            SetTxtValue(calculator, lblProbability);
        }

        public override string Text
        {
            get
            {
                return lblName.Text;
            }
            set
            {
                lblName.Text = value;
            }
        }

        public bool Checked
        {
            get
            {
                return chkUsing.Checked;
            }
            set
            {
                chkUsing.Checked = value;
                if (Changed != null)
                    Changed(this, EventArgs.Empty);
            }
        }

        public double GetProbability()
        {
            return Double.Parse(lblProbability.Text);
        }

        private void chkUsing_CheckedChanged(object sender, EventArgs e)
        {
            if (Changed != null)
                Changed(this, EventArgs.Empty);
        }
    }
}
