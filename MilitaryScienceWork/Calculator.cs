﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryScienceWork
{
    public class Calculator
    {
        public double A; // Константа А в формуле Pob
        public double B; // Константа B в формуле Pob
        public double Se; // Площадь элемента разрешения
        public double F; // Фокусное расстояние объектива АФР
        public double MinDn; // Минимальная дальность разведки
        public double MaxDn; // Максимальная дальность разведки
        public double Sob; // Площадь наблюдаемой проекции объекта
        public double Ro; // Коэффициент яркости объекта
        public double Rf; // Коэффициент яркости фона
        public double Rd; // Коэффициент яркости дымки
        public double Ta; // Коэффициент пропускания атмосферы
        public double Tob; // Значение пространственно-частотных характеристик объектива
        public double Tfp; // Значение пространственно-частотных характеристик фотопленки
        public double G; // Коэффициент контрастности

        public Calculator(double a=1, double b=0.15)
        {
            A = a;
            B = b;
        }

        //Возвращет вероятность обнаружения в зависимости от дальности разведки
        public double Pob(double Dn)
        {
            double X = Rd / Ta; //Коэффициент задымленности атмосферы
            double Su = (Sob <= 160 * Se) ? (Sob) : (160 * Se); //Площадь пространственного интегрирования
            double deltaD = G * Math.Abs(Math.Log(((Ro + Rf) / 2 + (Ro - Rf) / 2 * Tob * Tfp + X) / (Rf + X)));
            double Qb = 3 * 1000 * deltaD * F * Math.Sqrt(Su) / Dn; 
            if (Qb > 1)
            {
                return 1 - Math.Exp((-B * (Qb - A) * (Qb - A)));
            }
            else
            {
                //TODO Integral
                return 1 - Math.Exp((-B * (Qb - A) * (Qb - A)));
                //return 0;
            }
        }
    }
}
